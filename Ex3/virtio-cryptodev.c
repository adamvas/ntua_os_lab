/*
 * Virtio Cryptodev Device
 *
 * Implementation of virtio-cryptodev qemu backend device.
 *
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr> 
 * Konstantinos Papazafeiropoulos <kpapazaf@cslab.ece.ntua.gr>
 *
 */

#include "qemu/osdep.h"
#include "qemu/iov.h"
#include "hw/qdev.h"
#include "hw/virtio/virtio.h"
#include "standard-headers/linux/virtio_ids.h"
#include "hw/virtio/virtio-cryptodev.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <crypto/cryptodev.h>

static uint64_t get_features(VirtIODevice *vdev, uint64_t features,
		Error **errp)
{
	DEBUG_IN();
	return features;
}

static void get_config(VirtIODevice *vdev, uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_config(VirtIODevice *vdev, const uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_status(VirtIODevice *vdev, uint8_t status)
{
	DEBUG_IN();
}

static void vser_reset(VirtIODevice *vdev)
{
	DEBUG_IN();
}

static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq)
{
	VirtQueueElement *elem;
	unsigned int *syscall_type;

	DEBUG_IN();

	elem = virtqueue_pop(vq, sizeof(VirtQueueElement));
	if (!elem) {
		DEBUG("No item to pop from VQ :(");
		return;
	} 

	DEBUG("I have got an item from VQ :)");

	syscall_type = elem->out_sg[0].iov_base;
	int *cfd;
	//cfd = (int*)malloc(sizeof(*cfd));
	switch (*syscall_type) {

		case VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN");
			cfd =  elem->in_sg[0].iov_base;
			*cfd = open("/dev/crypto", O_RDWR);
			if (*cfd < 0) {
				DEBUG("could not open(/dev/crypto)");
			}
			DEBUG("Opened crypto dev");
			break;

		case VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE");
			cfd = elem->out_sg[1].iov_base;
			if(close(*cfd) < 0)
				DEBUG("could not close(/dev/crypto)");
			DEBUG("Closed crypto dev");
			break;

		case VIRTIO_CRYPTODEV_SYSCALL_TYPE_IOCTL:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_IOCTL");
			//variables for fields
			int *host_fd;
			int cfd;
			unsigned int *cmd;

			struct session_op *sess_op;
			struct session_op sess;
			struct crypt_op *cryp_op;
			struct crypt_op cryp;

			uint32_t *sess_id;
			uint32_t _sess_id;
			unsigned char *sess_key;
			unsigned char *iv, *src, *dst;
			
			//standard input from sgs
			host_fd = elem->out_sg[1].iov_base;
			cfd = *host_fd;
			cmd = elem->out_sg[2].iov_base;

			switch(*cmd){
			case CIOCGSESSION:
				DEBUG("CIOCGSESSION");
				//Read from sgs
				sess_key = elem->out_sg[3].iov_base;
				sess_op = elem->in_sg[0].iov_base;
				//Populate values
				memset(&sess, 0 ,sizeof(sess));
				sess.keylen = sess_op->keylen;
				sess.cipher = sess_op->cipher;
				sess.key = sess_key;

				if(ioctl(cfd, CIOCGSESSION, &sess) < 0)
					DEBUG("Failed to start session");
				sess_op->ses = sess.ses;
				break;
			case CIOCFSESSION:
				DEBUG("CIOCFSESSION");
				//Read from sgs
				sess_id = elem->out_sg[3].iov_base;
				//Populate value
				_sess_id = *sess_id;
				if(ioctl(cfd, CIOCFSESSION, &_sess_id) < 0)
					DEBUG("Failed to end session");
			
			case CIOCCRYPT:
				DEBUG("CIOCCRYPT");

				//Read from sgs
				cryp_op = elem->out_sg[3].iov_base;
				iv = elem->out_sg[4].iov_base;
				src = elem->out_sg[5].iov_base;
				dst = elem->in_sg[0].iov_base;
				//Populate values
				memset(&cryp, 0, sizeof(cryp));
				cryp.ses = cryp_op->ses;
				cryp.len = cryp_op->len;
				cryp.iv = iv;
				cryp.src = src;
				cryp.dst = dst;
				cryp.op = cryp_op->op;

				if(ioctl(cfd, CIOCCRYPT, &cryp) < 0)
					DEBUG("Failed to do cryp operation");
				
			default: break;
			}
		break;	

		default:
			DEBUG("Unknown syscall_type");
			break;

	}

	virtqueue_push(vq, elem, 0);
	virtio_notify(vdev, vq);
	g_free(elem);
}

static void virtio_cryptodev_realize(DeviceState *dev, Error **errp)
{
	VirtIODevice *vdev = VIRTIO_DEVICE(dev);

	DEBUG_IN();

	virtio_init(vdev, "virtio-cryptodev", VIRTIO_ID_CRYPTODEV, 0);
	virtio_add_queue(vdev, 128, vq_handle_output);
}

static void virtio_cryptodev_unrealize(DeviceState *dev, Error **errp)
{
	DEBUG_IN();
}

static Property virtio_cryptodev_properties[] = {
	DEFINE_PROP_END_OF_LIST(),
};

static void virtio_cryptodev_class_init(ObjectClass *klass, void *data)
{
	DeviceClass *dc = DEVICE_CLASS(klass);
	VirtioDeviceClass *k = VIRTIO_DEVICE_CLASS(klass);

	DEBUG_IN();
	dc->props = virtio_cryptodev_properties;
	set_bit(DEVICE_CATEGORY_INPUT, dc->categories);

	k->realize = virtio_cryptodev_realize;
	k->unrealize = virtio_cryptodev_unrealize;
	k->get_features = get_features;
	k->get_config = get_config;
	k->set_config = set_config;
	k->set_status = set_status;
	k->reset = vser_reset;







}

static const TypeInfo virtio_cryptodev_info = {
	.name          = TYPE_VIRTIO_CRYPTODEV,
	.parent        = TYPE_VIRTIO_DEVICE,
	.instance_size = sizeof(VirtCryptodev),
	.class_init    = virtio_cryptodev_class_init,
};

static void virtio_cryptodev_register_types(void){
	type_register_static(&virtio_cryptodev_info);
}

type_init(virtio_cryptodev_register_types)
