#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <crypto/cryptodev.h>


//#define TCP_PORT    35001
#define TCP_BACKLOG 5

#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE        16  /* AES128 */

struct session_op sess;
struct crypt_op cryp;
int cfd;
/* initialization vector for crypt operation */
char in_vec[BLOCK_SIZE];       //must initialize
/* key for crypt operations */
char key[KEY_SIZE];          //must initialize




void print(char *a, int s){
	for(int i = 0; i < s; i++){
		printf("%c", a[i]);
	}
	printf("\n");
}

ssize_t insist_read(int fd, void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
		ret = read(fd, buf, cnt);
		if (ret < 0)
			return ret;
		buf += ret;
		cnt -= ret;
	}

	return orig_cnt;
}



int encrypt(char *in, int len, char *dest){
	/** CRYPT SETUP START **/
        cryp.ses = sess.ses;            //session for operation
        cryp.len = len;
        cryp.src = in;
        cryp.dst = dest;
        cryp.iv = in_vec;
        cryp.op = COP_ENCRYPT;
        /** CRYPT SETUP END **/
	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
                perror("ioctl(CIOCCRYPT)");
                return -1;
        }
}



int decrypt(char *in, int len, char *dest){
	/** CRYPT SETUP START **/
	cryp.ses = sess.ses;            //session for operation
	cryp.len = len;
	cryp.src = in;
	cryp.dst = dest;
	cryp.iv = in_vec;
	cryp.op = COP_DECRYPT;
	/** CRYPT SETUP END **/
	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
			perror("ioctl(CIOCCRYPT)");
			return -1;
	}
}




/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = write(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

void communicate(int sd,uint8_t clientorserver);

int main(int argc, char *argv[]){
    //cryptodev associated things below
    memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
    memset(in_vec, 42, sizeof(in_vec));
	memset(key, 17, sizeof(key));
    
    /** INIT SESSION **/
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = key;
	cfd = open("/dev/crypto", O_RDWR);
	if (cfd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}
	/** INIT SESSION END **/

    
    
    //socket asssociated things below
    int sd, newsd, port;
    int cli_or_srv = 0;
    ssize_t n;
    char buf[100];
    char sendBuf[253];
    char addrstr[INET_ADDRSTRLEN];
    char *hostname;
    socklen_t len;
    struct hostent *hp;
    struct sockaddr_in sa;
    signal(SIGPIPE, SIG_IGN);

    if (argc != 3) {
            fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
            exit(1);
    }
    hostname = argv[1];
    port = atoi(argv[2]); /* Needs better error checking */

    /* Create TCP/IP socket, used as main chat channel */
    if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket");
            exit(1);
    }

    if ( !(hp = gethostbyname(hostname))) {
            printf("DNS lookup failed for host %s\n", hostname);
            exit(1);
    }

    /** CLIENT START **/
    /* Connect to remote TCP port */
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
    //fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
    if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
            printf("User is down, waiting...\n");
            goto TRY_SERVER;
    }
    cli_or_srv = 0;
    goto COMMUNICATE;
    /** CLIENT END **/
    
    /** SERVER START **/
    TRY_SERVER:
    /* Bind to a well-known port */
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
            perror("bind");
            exit(1);
    }
    //fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

    /* Listen for incoming connections */
    if (listen(sd, TCP_BACKLOG) < 0) {
            perror("listen");
            exit(1);
    }
    /* Accept when the other user attempts to connect */
    if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
            perror("accept");
            exit(1);
    }
    if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
            perror("could not format IP address");
            exit(1);
    }
    //fprintf(stderr, "Incoming connection from %s:%d\n", addrstr, ntohs(sa.sin_port));
    cli_or_srv = 1;
    goto COMMUNICATE;
    /** SERVER END **/
    
    COMMUNICATE:
    fprintf(stderr, "- - -Chat started- - -\n");
    if (cli_or_srv ==1) communicate(newsd, cli_or_srv);
    else communicate(sd,cli_or_srv);
    return 0;
} 

void listener_task(int sd);
void sender_task(int sd);

void communicate(int sd, uint8_t clientorserver){ //0= client , 1 = server

  pid_t listener = fork();
  if (listener == 0){
    listener_task(sd);
   return;
  }
  else if (listener<0){
      perror("fork for  listener");
      exit(1);
  }

    pid_t sender = fork();
    if (sender ==0){
        sender_task(sd);
        return;
    }
    else if (sender<0){
        perror("fork for sender");
        exit(1);
    }
    wait(NULL);
    if (clientorserver==0)shutdown(sd, SHUT_RDWR);
    else if (clientorserver == 1){
		sleep(0.1);
		close(sd);
	}
    kill(listener, SIGKILL);
    kill(sender, SIGKILL);
}
  
void sender_task(int sd){
     ssize_t n;
     char buf[DATA_SIZE]; 
     char endmess[5];
     char encrypted[DATA_SIZE];
     strcpy(endmess,"END\n");
     
     for (;;) {
            n = fgets(buf, DATA_SIZE, stdin);
            
            if (n <= 0) {
                if (n < 0)
                    perror("read from user input failed");
            }
            //n = strlen(buf);
            //printf("Read %d from keyboard\n", n);
            encrypt(buf,sizeof(buf),encrypted);
            if (insist_write(sd,encrypted,sizeof(encrypted) ) != sizeof(encrypted)) {
                perror("cannot write to standard output");
                break;
            }
            if (strcmp(buf,endmess) == 0 ) break;
        }
   
}
    
void listener_task(int sd){
    ssize_t n;
    char buf[DATA_SIZE];
    char decrypted[DATA_SIZE]; 
    char endmess[5];
    strcpy(endmess,"END\n");
    char responcemess[20];
    strcpy(responcemess,"    Responce:\0");

    for (;;) {
        n = insist_read(sd, buf, DATA_SIZE);
        if (n <= 0) {
            if (n < 0)
                perror("read from remote peer failed");
            else
                fprintf(stderr, "Peer went away\n");
            break;
        }
        decrypt(buf,sizeof(buf),decrypted);//ignore the last one since we added it (\0).....
        if (insist_write(0, responcemess, strlen(responcemess)) != strlen(responcemess)) {
            perror("cannot write to standard output");
            break;
        }
        if (insist_write(0, decrypted, strlen(decrypted)) != strlen(decrypted)) {
            perror("cannot write to standard output");
            break;
        }
        
        if (strcmp(decrypted,endmess) == 0 ) break;

    }
}





