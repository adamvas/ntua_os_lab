[//]: <> (pandoc --pdf-engine=xelatex --highlight-style tango anafora.md -o anafora.pdf -V mainfont="DejaVu Serif" -V monofont="DejaVu Sans Mono" -V geometry:"top=3cm, bottom=2.3cm, left=2.5cm, right=2.5cm")

---
header-includes:
 - \usepackage{fvextra}
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
 
 
 - \definecolor{bgcolor}{HTML}{f8f8f8}
 - \let\oldtexttt\texttt

 - \renewcommand{\texttt}[1]{
  \colorbox{bgcolor}{\oldtexttt{#1}}
  } 
  

 - \hypersetup{colorlinks=false,
            allbordercolors={0 0 0.5},
            pdfborderstyle={/S/U/W 1}}
 

 - \renewenvironment{Shaded} {\begin{snugshade}\small} {\end{snugshade}}
---

![ ](./pyrforos.png )

# Εθνικό Μετσόβιο Πολυτεχνείο 
## Σχολή Ηλεκτρολόγων Μηχανικών και Μηχανικών Υπολογιστών 
## Εργαστήριο Λειτουργικών Συστημάτων, 2019-2020 

***

## 3η Εργαστηριακή Άσκηση:
## Κρυπτογραφική συσκευή VirtIO για QEMU-KVM
#### Ονοματεπώνυμα: Αδαμόπουλος Βασίλειος, Τσαγκαρέλης Γεώργιος
#### Α.Μ.: 03116695, 03116674

***

Στο πρώτο μέρος αυτής της εργασίας υλοποιήθηκε μία εφαρμογή χρήστη για την ανταλλαγή μυνημάτων μέσω sockets αρχικώς ακρυπτογράφητα. Στην συνέχεια, στην επόμενη έκδοχη της, τα μυνηματα κρυπτόγραφούνται με βάση προσυμφωνημένα κλειδιά πριν την αποστολή τους, με τη χρήση της κρυπτογραφικής συσκευής cryptodev.

Στο δεύτερο μέρος υλοποιείται, με χρήση της τεχνικής της παραεικονοποίησης(Virtio API), μία εικονική κρυπτογραφική συσκευή. Με αυτό τον τρόπο δύνεται η δυνατότητα σε προγράμματα που εκτελούνται μέσα σε μια εικονική συσκευή (VM) να αξιοποιούν το πραγματικό hardware της συσκευής στην οποία εκτελείται το VM.


## Περιεχόμενα:

1. [Ζ1: Εργαλείο chat πάνω από TCP/IP sockets](#label1)
2. [Ζ2: Κρυπτογραφημένο chat πάνω από TCP/IP ](#label2)
3. [Ζ3: Υλοποίηση συσκευής cryptodev με VirtIO ](#label3)
    * [Device Driver κρυπτογραφικής συσκευής (Module στον Kernel του guest os) ](#label4) 
    * [Προσθήκη στον κώδικα του QEMU (host) για την υποστήριξη του device driver που τρέχει στον quest](#label5)


***

### Ζ1: Εργαλείο chat πάνω από TCP/IP sockets {#label1}


```C
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#define TCP_PORT    35001
#define TCP_BACKLOG 5



/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = write(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

void communicate(int sd,uint8_t clientorserver);

int main(int argc, char *argv[]){
    int sd, newsd, port;
    int cli_or_srv = 0;
    ssize_t n;
    char buf[100];
    char sendBuf[253];
    char addrstr[INET_ADDRSTRLEN];
    char *hostname;
    socklen_t len;
    struct hostent *hp;
    struct sockaddr_in sa;
    signal(SIGPIPE, SIG_IGN);

    if (argc != 3) {
            fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
            exit(1);
    }
    hostname = argv[1];
    port = atoi(argv[2]); /* Needs better error checking */

    /* Create TCP/IP socket, used as main chat channel */
    if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket");
            exit(1);
    }

    if ( !(hp = gethostbyname(hostname))) {
            printf("DNS looku<p failed for host %s\n", hostname);
            exit(1);
    }

    /** CLIENT START **/
    /* Connect to remote TCP port */
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
    //fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
    if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
            printf("User is down, waiting...\n");
            goto TRY_SERVER;
    }
    cli_or_srv = 0;
    goto COMMUNICATE;
    /** CLIENT END **/
    
    /** SERVER START **/
    TRY_SERVER:
    /* Bind to a well-known port */
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
            perror("bind");
            exit(1);
    }
    //fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

    /* Listen for incoming connections */
    if (listen(sd, TCP_BACKLOG) < 0) {
            perror("listen");
            exit(1);
    }
    /* Accept when the other user attempts to connect */
    if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
            perror("accept");
            exit(1);
    }
    if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
            perror("could not format IP address");
            exit(1);
    }
    //fprintf(stderr, "Incoming connection from %s:%d\n", addrstr, ntohs(sa.sin_port));
    cli_or_srv = 1;
    goto COMMUNICATE;
    /** SERVER END **/
    
    COMMUNICATE:
    fprintf(stderr, "- - -Chat started- - -\n");
    if (cli_or_srv ==1) communicate(newsd, cli_or_srv);
    else communicate(sd,cli_or_srv);
    return 0;
} 

void listener_task(int sd);
void sender_task(int sd);

void communicate(int sd, uint8_t clientorserver){ //0= client , 1 = server

  pid_t listener = fork();
  if (listener == 0){
    listener_task(sd);
   return;
  }
  else if (listener<0){
      perror("fork for  listener");
      exit(1);
  }

    pid_t sender = fork();
    if (sender ==0){
        sender_task(sd);
        return;
    }
    else if (sender<0){
        perror("fork for sender");
        exit(1);
    }
    wait(NULL);
    if (clientorserver==0)shutdown(sd, SHUT_RDWR);
    else if (clientorserver == 1){
		sleep(0.1);
		close(sd);
	}
    kill(listener, SIGKILL);
    kill(sender, SIGKILL);
}
  
void sender_task(int sd){
     char *n;
     char buf[100]; 
     char endmess[5];
     strcpy(endmess,"END\n");
     
     for (;;) {
            n = fgets(buf, 100, stdin);
            
            if (n == NULL) {
                    perror("read from user input failed");
            }
            int l = strlen(buf);
            //printf("Read %d from keyboard\n", n);
            if (insist_write(sd, buf,l) != l) {
                perror("cannot write to standard output");
                break;
            }
            if (strcmp(buf,endmess) == 0 ) break;
        }
   
}
    
void listener_task(int sd){
    ssize_t n;
    char buf[100]; 
    char endmess[5];
    strcpy(endmess,"END\n");
    char responcemess[20];
    strcpy(responcemess,"    Responce:\0");

    for (;;) {
        n = read(sd, buf, 100);
        if (n <= 0) {
            if (n < 0)
                perror("read from remote peer failed");
            else
                fprintf(stderr, "Peer went away\n");
            break;
        }
        if (insist_write(0, responcemess, strlen(responcemess)) != strlen(responcemess)) {
            perror("cannot write to standard output");
            break;
        }
        if (insist_write(0, buf, n) != n) {
            perror("cannot write to standard output");
            break;
        }
        
        if (strcmp(buf,endmess) == 0 ) break;

    }
}

```

### Ζ2: Κρυπτογραφημένο chat πάνω από TCP/IP {#label2}


```C
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <crypto/cryptodev.h>


//#define TCP_PORT    35001
#define TCP_BACKLOG 5

#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE        16  /* AES128 */

struct session_op sess;
struct crypt_op cryp;
int cfd;
/* initialization vector for crypt operation */
char in_vec[BLOCK_SIZE];       //must initialize
/* key for crypt operations */
char key[KEY_SIZE];          //must initialize




void print(char *a, int s){
	for(int i = 0; i < s; i++){
		printf("%c", a[i]);
	}
	printf("\n");
}

ssize_t insist_read(int fd, void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
		ret = read(fd, buf, cnt);
		if (ret < 0)
			return ret;
		buf += ret;
		cnt -= ret;
	}

	return orig_cnt;
}



int encrypt(char *in, int len, char *dest){
	/** CRYPT SETUP START **/
        cryp.ses = sess.ses;            //session for operation
        cryp.len = len;
        cryp.src = in;
        cryp.dst = dest;
        cryp.iv = in_vec;
        cryp.op = COP_ENCRYPT;
        /** CRYPT SETUP END **/
	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
                perror("ioctl(CIOCCRYPT)");
                return -1;
        }
}



int decrypt(char *in, int len, char *dest){
	/** CRYPT SETUP START **/
	cryp.ses = sess.ses;            //session for operation
	cryp.len = len;
	cryp.src = in;
	cryp.dst = dest;
	cryp.iv = in_vec;
	cryp.op = COP_DECRYPT;
	/** CRYPT SETUP END **/
	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
			perror("ioctl(CIOCCRYPT)");
			return -1;
	}
}




/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = write(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

void communicate(int sd,uint8_t clientorserver);

int main(int argc, char *argv[]){
    //cryptodev associated things below
    memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
    memset(in_vec, 42, sizeof(in_vec));
	memset(key, 17, sizeof(key));
    
    /** INIT SESSION **/
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = key;
	cfd = open("/dev/crypto", O_RDWR);
	if (cfd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}
	/** INIT SESSION END **/

    
    
    //socket asssociated things below
    int sd, newsd, port;
    int cli_or_srv = 0;
    ssize_t n;
    char buf[100];
    char sendBuf[253];
    char addrstr[INET_ADDRSTRLEN];
    char *hostname;
    socklen_t len;
    struct hostent *hp;
    struct sockaddr_in sa;
    signal(SIGPIPE, SIG_IGN);

    if (argc != 3) {
            fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
            exit(1);
    }
    hostname = argv[1];
    port = atoi(argv[2]); /* Needs better error checking */

    /* Create TCP/IP socket, used as main chat channel */
    if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket");
            exit(1);
    }

    if ( !(hp = gethostbyname(hostname))) {
            printf("DNS lookup failed for host %s\n", hostname);
            exit(1);
    }

    /** CLIENT START **/
    /* Connect to remote TCP port */
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
    //fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
    if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
            printf("User is down, waiting...\n");
            goto TRY_SERVER;
    }
    cli_or_srv = 0;
    goto COMMUNICATE;
    /** CLIENT END **/
    
    /** SERVER START **/
    TRY_SERVER:
    /* Bind to a well-known port */
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
            perror("bind");
            exit(1);
    }
    //fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

    /* Listen for incoming connections */
    if (listen(sd, TCP_BACKLOG) < 0) {
            perror("listen");
            exit(1);
    }
    /* Accept when the other user attempts to connect */
    if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
            perror("accept");
            exit(1);
    }
    if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
            perror("could not format IP address");
            exit(1);
    }
    //fprintf(stderr, "Incoming connection from %s:%d\n", addrstr, ntohs(sa.sin_port));
    cli_or_srv = 1;
    goto COMMUNICATE;
    /** SERVER END **/
    
    COMMUNICATE:
    fprintf(stderr, "- - -Chat started- - -\n");
    if (cli_or_srv ==1) communicate(newsd, cli_or_srv);
    else communicate(sd,cli_or_srv);
    return 0;
} 

void listener_task(int sd);
void sender_task(int sd);

void communicate(int sd, uint8_t clientorserver){ //0= client , 1 = server

  pid_t listener = fork();
  if (listener == 0){
    listener_task(sd);
   return;
  }
  else if (listener<0){
      perror("fork for  listener");
      exit(1);
  }

    pid_t sender = fork();
    if (sender ==0){
        sender_task(sd);
        return;
    }
    else if (sender<0){
        perror("fork for sender");
        exit(1);
    }
    wait(NULL);
    if (clientorserver==0)shutdown(sd, SHUT_RDWR);
    else if (clientorserver == 1){
		sleep(0.1);
		close(sd);
	}
    kill(listener, SIGKILL);
    kill(sender, SIGKILL);
}
  
void sender_task(int sd){
     char *n;
     char buf[DATA_SIZE]; 
     char endmess[5];
     char encrypted[DATA_SIZE];
     strcpy(endmess,"END\n");
     
     for (;;) {
            n = fgets(buf, DATA_SIZE, stdin);
            
            if (n ==NULL ) {
                    perror("read from user input failed");
            }
            //n = strlen(buf);
            //printf("Read %d from keyboard\n", n);
            encrypt(buf,sizeof(buf),encrypted);
            if (insist_write(sd,encrypted,sizeof(encrypted) ) != sizeof(encrypted)) {
                perror("cannot write to standard output");
                break;
            }
            if (strcmp(buf,endmess) == 0 ) break;
        }
   
}
    
void listener_task(int sd){
    ssize_t n;
    char buf[DATA_SIZE];
    char decrypted[DATA_SIZE]; 
    char endmess[5];
    strcpy(endmess,"END\n");
    char responcemess[20];
    strcpy(responcemess,"    Responce:\0");

    for (;;) {
        n = insist_read(sd, buf, DATA_SIZE);
        if (n <= 0) {
            if (n < 0)
                perror("read from remote peer failed");
            else
                fprintf(stderr, "Peer went away\n");
            break;
        }
        decrypt(buf,sizeof(buf),decrypted);//ignore the last one since we added it (\0).....
        if (insist_write(0, responcemess, strlen(responcemess)) != strlen(responcemess)) {
            perror("cannot write to standard output");
            break;
        }
        if (insist_write(0, decrypted, strlen(decrypted)) != strlen(decrypted)) {
            perror("cannot write to standard output");
            break;
        }
        
        if (strcmp(decrypted,endmess) == 0 ) break;

    }
}

```

### Ζ3: Υλοποίηση συσκευής cryptodev με VirtIO {#label3}

Η υλοποίηση της εικονικής συσκευής απαρτίζεται από δύο μέρη. 

* Μία συσκευή χαρκτηρων η οποία "ζει" στην εικονική μηχανη (guest) η οποία διατηρεί ακριβώς την ίδια διεπαφή με το χρήστη, οπως στον κανονικό οδηγό. Πρέπει όμως παράλληλα να επικοινωνεί με το host λειρουργικό σύστημα ώστε να του παραδίδει τις απαιτούμενες κλήσεις προς το cryptodevice.

* Προσθήκη της υποστήριξης της εικονικής συσκευής μέσα στον κώδικα του QEMU ώστε να μπορεί να επικοινωνεί με τον οδηγό του guest και να εκπληρώνει τα αιτήματα του (κλήσεις προς την πραγματική συσκευή cryptodev).

#### Device Driver κρυπτογραφικής συσκευής (Module στον Kernel του guest os) {#label4}


Υποστηρίζει τις ίδιες κλήσεις με την κανονική συσκευή /dev/crypto.
Κάθε κλήση πρέπει να διαβιβαστεί στο host οs. Αυτό γίνεται μέσω του Virtio API. Τα δεδομένα που χρειάζεται o host για να εξυπηρετήσει τυποποιούνται μέσα σε scatter-gather λιστες και τοποθετούνται στον ring buffer που μοιράζεται ο οδηγός με το backend που τρέχει στο QEMU. Επειδή όμως οι διευθύνσεις του guest αναφέρονται σε άλλο address space σε σχεση με τον host (όπου τρέχει το QEMU) οι δείκτες δεν έχουν νόημα για τον host. Έτσι λοιπόν όταν θέλουμε να περάσουμε structs που περιέχουν δείκτες δεν αρκεί να περάσουμε μόνο την διεύθυνση του struct αλλα αναγκαζόμαστε να περάσουμε και τις διευθύνσεις των επιμέρους στοιχείων στα οποία δείχνουν οι δείκτες του struct. Για παράδειγμα αυτό συνέβη όταν έπρεπε να περάσουμε το struct crypt_op κλήση IOCTL CCRYPT, το οποίο έχει τη ακόλουθη δομή:
```C
/* input of CIOCCRYPT */
struct crypt_op {
	__u32	ses;		/* session identifier */
	__u16	op;		/* COP_ENCRYPT or COP_DECRYPT */
	__u16	flags;		/* see COP_FLAG_* */
	__u32	len;		/* length of source data */
	__u8	__user *src;	/* source data */
	__u8	__user *dst;	/* pointer to output data */
	/* pointer to output data for hash/MAC operations */
	__u8	__user *mac;
	/* initialization vector for encryption operations */
	__u8	__user *iv;
};
```
Απόσπασμα της ioctl στο οποίο αντιγράφουμε στη scatter-gather λιστα και τις διευθύνσεις στις οποίες δείχνουν δείκτες του struct:

```C
	case CIOCCRYPT:
		debug("CIOCCRYPT");
		if(copy_from_user(cryp_op, (void __user *)arg, sizeof(*cryp_op)))
			return -EFAULT;
		iv = kmalloc(VIRTIO_CRYPTODEV_BLOCK_SIZE, GFP_KERNEL);
		src = kmalloc(cryp_op->len, GFP_KERNEL);
		dst = kmalloc(cryp_op->len, GFP_KERNEL);
		//populate arrays from user space
		if(copy_from_user(iv, cryp_op->iv, VIRTIO_CRYPTODEV_BLOCK_SIZE))
			return -EFAULT;
		if(copy_from_user(src, cryp_op->src, cryp_op->len))
			return -EFAULT;
		if(copy_from_user(dst, cryp_op->dst, cryp_op->len))
			return -EFAULT;
		//init specific sg
		sg_init_one(&cryp_op_sg, cryp_op, sizeof(*cryp_op));
		sg_init_one(&iv_sg, iv, VIRTIO_CRYPTODEV_BLOCK_SIZE);
		sg_init_one(&src_sg, src, cryp_op->len);
		sg_init_one(&dst_sg, dst, cryp_op->len);
		
		//attach specific sg
		sgs[3] = &cryp_op_sg;
		sgs[4] = &iv_sg;
		sgs[5] = &src_sg;
		sgs[6] = &dst_sg;
```

Ένα ακόμη κρίσιμο κομμάτι της υλοποίησησς είναι η χρήση σημαφόρων ώστε να διασφαλιστεί ότι δεν θα γράψουν/διαβάσουν δυο διαφορετικές διεργασίες ταυτόχρονα στην Virtqueue. Αυτό είναι σημαντικό διότι η Virtqueue είναι μοναδική για το device, επομένως για οποιαδήποτε διεργασία προσπαθήσει να κάνει χρήση της συσκευής ο οδηγός θα τοποθετήσει τα δεδομένα μέσα στην ίδια Virtqueue χωρίς να έχει τρόπο ( με την παρούσα υλοποίηση) να ξεχωρίσει ποιά διεργασία τοποθέτησε κάτι στην Virtqueue.


Απόσπασμα κώδικα στο οποίο φαίνεται το κλείδωμα εως ότου έρθει η απάντηση από τον host:

```C
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;
	err = virtqueue_add_sgs(vq,sgp,2,0,&syscall_type_sg,GFP_ATOMIC);
	virtqueue_kick(vq);
	while(virtqueue_get_buf(vq,&len) == NULL);
	debug("finished closing the crypto fd");
	up(&crdev->sem);
```
και η δήλωση του σεμαφόρου:
```C
struct crypto_device {
	/* Next crypto device in the list, head is in the crdrvdata struct */
	struct list_head list;

	/* The virtio device we are associated with. */
	struct virtio_device *vdev;

	struct virtqueue *vq;
	struct semaphore sem;

	/* The minor number of the device. */
	unsigned int minor;
};

```


Ο πλήρης κώδικας της υλοποίησης των open(),release() και ioctl():



```C

static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev;
	struct virtqueue *vq;
	unsigned int *syscall_type;
	int *host_fd;
	struct scatterlist syscall_type_sg, cfd_sg, *sgp[2];
	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_OPEN;
	host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = -1; //initialize the reply to -1 in case you dont get answer
	
	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));
	if (!crdev) {
		debug("Could not find crypto device with %u minor", 
		      iminor(inode));
		ret = -ENODEV;
		goto fail;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail;
	}

	//This is how we get the message back to end user
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;
	vq = crdev->vq;
	//sema_init(&crdev->sem, 1);

	/**
	 * We need two sg lists, one for syscall_type and one to get the 
	 * file descriptor from the host.
	 **/
	/* ?? */

	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	//sg_init_one(&cfd_sg, cfd, sizeof(cfd));
	sg_init_one(&cfd_sg,host_fd,sizeof(*host_fd));

	sgp[0] = &syscall_type_sg;
	sgp[1] = &cfd_sg;
	//lock
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;

	err = virtqueue_add_sgs(vq, sgp, 1, 1, &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);

	/**
	 * Wait for the host to process our data.
	 **/
	while(virtqueue_get_buf(vq, &len) == NULL);
	debug("The true fd is: %d", *host_fd);
	up(&crdev->sem);
	//end lock
	crof->host_fd = *host_fd;
	/* ?? */


	/* If host failed to open() return -ENODEV. */
	/* ?? */
		

fail:
	kfree(syscall_type);
	kfree(host_fd);
	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	unsigned int *syscall_type;
	int *host_fd;
	struct scatterlist syscall_type_sg, cfd_sg, *sgp[2];
	struct virtqueue *vq;
	debug("Entering");

	vq = crdev->vq;
	
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_CLOSE;

	host_fd = kzalloc(sizeof(*host_fd),GFP_KERNEL);
	*host_fd = crof->host_fd;
	
	sg_init_one(&syscall_type_sg,syscall_type,sizeof(syscall_type));
	sg_init_one(&cfd_sg,host_fd,sizeof(*host_fd));
	
	sgp[0]=&syscall_type_sg;
	sgp[1]=&cfd_sg;
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;

	err = virtqueue_add_sgs(vq,sgp,2,0,&syscall_type_sg,GFP_ATOMIC);
	virtqueue_kick(vq);
	
	while(virtqueue_get_buf(vq,&len) == NULL);
	debug("finished closing the crypto fd");

	up(&crdev->sem);
	/**
	 * Send data to the host.
	 **/
	/* ?? */

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */

	//kfree(crof);    ???this was here but i read that  you should use kfree only after kmalloc
	kfree(syscall_type);
	kfree(host_fd);
	debug("Leaving");
	return ret;

}

static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd, 
                                unsigned long arg)
{
	long ret = 0;
	int err;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	unsigned int num_in, num_out;

	//Variables for fields
	struct session_op *sess_op;
	struct crypt_op *cryp_op;
	
	unsigned int len;

	unsigned int *syscall_type;
	unsigned int *mycmd;
	int *cfd;
	
	unsigned char *sess_key;
	u_int32_t *sess_id;
	unsigned char *iv, *dst, *src;

	//Their scatterlists
	struct scatterlist syscall_type_sg, cfd_sg, mycmd_sg;
	struct scatterlist sess_id_sg, sess_op_sg, sess_key_sg;
	struct scatterlist cryp_op_sg, iv_sg, src_sg, dst_sg;
	struct scatterlist *sgs[10];

	debug("Entering");

	/**
	 * Standard Inits
	 **/
	num_in = 0;
	num_out = 0;
	syscall_type = kmalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_IOCTL;

	mycmd = kmalloc(sizeof(*mycmd), GFP_KERNEL);
	*mycmd = cmd;

	cfd = kmalloc(sizeof(*cfd), GFP_KERNEL);
	*cfd = crof->host_fd;

	/**
	 * Extra Inits
	 **/
	sess_op = kzalloc(sizeof(*sess_op), GFP_KERNEL);
	sess_id = kzalloc(sizeof(*sess_id), GFP_KERNEL);
	sess_key = kmalloc(sizeof(unsigned char), GFP_KERNEL);

	cryp_op = kzalloc(sizeof(*cryp_op), GFP_KERNEL);
	
	
	iv = kzalloc(sizeof(unsigned char), GFP_KERNEL);
	src = kzalloc(sizeof(unsigned char), GFP_KERNEL);
	dst = kzalloc(sizeof(unsigned char), GFP_KERNEL);

	
	/**
	 * Standard sg Inits
	 **/
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sg_init_one(&cfd_sg, cfd, sizeof(*cfd));
	sg_init_one(&mycmd_sg, mycmd, sizeof(*mycmd));

	/**
	 * Standard sg attaches
	 **/
	sgs[0] = &syscall_type_sg;
	sgs[1] = &cfd_sg;
	sgs[2] = &mycmd_sg;
	

	switch (*mycmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		//copy struct from userspace
		if(copy_from_user(sess_op, (void __user *)arg, sizeof(*sess_op)))
				return -EFAULT;
		//copy array from userspace using the struct pointer
		kfree(sess_key);
		sess_key = kmalloc(sess_op->keylen, GFP_KERNEL);
		if(copy_from_user(sess_key, sess_op->key, sess_op->keylen))
				return -EFAULT;
		//init specific sg
		sg_init_one(&sess_key_sg, sess_key, sess_op->keylen);
		sg_init_one(&sess_op_sg, sess_op, sizeof(*sess_op));
		//attach specifig sg
		sgs[3] = &sess_key_sg;
		sgs[4] = &sess_op_sg; //in

		//set out-in values
		num_out = 4;
		num_in = 1;

		break;

	case CIOCFSESSION:
		debug("CIOCFSESSION");
		if(copy_from_user(sess_id, (void __user *)arg, sizeof(*sess_id)))
			return -EFAULT;

		sg_init_one(&sess_id_sg, sess_id, sizeof(*sess_id));
		sgs[3] = &sess_id_sg;

		//set out-in values
		num_out = 4;
		num_in = 0;

		break;
		
	case CIOCCRYPT:
		debug("CIOCCRYPT");
		if(copy_from_user(cryp_op, (void __user *)arg, sizeof(*cryp_op)))
			return -EFAULT;
		//init specific arrays
		kfree(iv);
		kfree(src);
		kfree(dst);		
		
		iv = kmalloc(VIRTIO_CRYPTODEV_BLOCK_SIZE, GFP_KERNEL);
		src = kmalloc(cryp_op->len, GFP_KERNEL);
		dst = kmalloc(cryp_op->len, GFP_KERNEL);
		//populate arrays from user space
		if(copy_from_user(iv, cryp_op->iv, VIRTIO_CRYPTODEV_BLOCK_SIZE))
			return -EFAULT;
		if(copy_from_user(src, cryp_op->src, cryp_op->len))
			return -EFAULT;
		if(copy_from_user(dst, cryp_op->dst, cryp_op->len))
			return -EFAULT;
		debug("Userspace src: %s", src);
		//init specific sg
		sg_init_one(&cryp_op_sg, cryp_op, sizeof(*cryp_op));
		sg_init_one(&iv_sg, iv, VIRTIO_CRYPTODEV_BLOCK_SIZE);
		sg_init_one(&src_sg, src, cryp_op->len);
		sg_init_one(&dst_sg, dst, cryp_op->len);
		
		//attach specific sg
		sgs[3] = &cryp_op_sg;
		sgs[4] = &iv_sg;
		sgs[5] = &src_sg;
		sgs[6] = &dst_sg;
		
		//set out-in values
		num_out = 6;
		num_in = 1;
		break;

	default:
		debug("Unsupported ioctl command");

		break;
	}


	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	/* ?? Lock ?? */
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;
	err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		/* do nothing */;
	up(&crdev->sem);

	switch(*mycmd){
		case CIOCGSESSION:
			if(copy_to_user((void __user *)arg, sess_op, sizeof(*sess_op)))
				return -EFAULT;
		case CIOCFSESSION:
			break;
		case CIOCCRYPT:
			if (copy_to_user(cryp_op->dst, dst, cryp_op->len))
				return -EFAULT;
		default :
			break;
	}
	kfree(syscall_type);
	kfree(mycmd);
	kfree(cfd);
	kfree(sess_op);
	kfree(sess_id);
	kfree(sess_key);
	kfree(cryp_op);
	kfree(iv);
	kfree(src);
	kfree(dst);
	debug("Leaving");

	return ret;
}
```

#### Προσθήκη στον κώδικα του QEMU (host) για την υποστήριξη του device driver που τρέχει στον quest {#label5}

Η λειτουργία του back-end ακολουθεί μία πιο απλή διαδικασία.

Όταν μια διεργασία προσθέσει κάτι στο virtqueue τότε καλείται η συνάρτηση 

```C 
static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq) 
```
Η συνάρτηση αυτή παίρνει ενα element struct από το virtqueue και ανάλογα με το syscall που αυτό θέλει να εκτελέσει, ακολουθεί την αντίστοιχη διαδικασία.
```C
syscall_type = elem->out_sg[0].iov_base; 
```

Σημαντικό σημείο στην εξυπηρέτηση των κλήσεων είναι η σύμβαση για τα σειρά και τα περιεχόμενα των scatterlists.

Με τη χρήση ενός switch διαχωρίζουμε τις περιπτώσεις κλήσεων.

Για την περίπτωση του OPEN: 
```C
case VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN");
			cfd =  elem->in_sg[0].iov_base;
			*cfd = open("/dev/crypto", O_RDWR);
			if (*cfd < 0) {
				DEBUG("could not open(/dev/crypto)");
			}
			DEBUG("Opened crypto dev");
			break;
```

Για την περίπτωση του CLOSE:
```C
case VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE");
			cfd = elem->out_sg[1].iov_base;
			if(close(*cfd) < 0)
				DEBUG("could not close(/dev/crypto)");
			DEBUG("Closed crypto dev");
			break;
```

Για την εκτέλεση IOCTL υπάρχει μια πιο σύνθετη, αλλά αρκετά παρόμοια διαδικασία.
Αρχικά δηλώνουμε τις απαραίτητες μεταβλητές, και αντιγράφουμε τα απαραίτητα για όλες τις υπο-περιπτώσεις πεδία
```C
int *host_fd;
			int cfd;
			unsigned int *cmd;

			struct session_op *sess_op;
			struct session_op sess;
			struct crypt_op *cryp_op;
			struct crypt_op cryp;

			uint32_t *sess_id;
			uint32_t _sess_id;
			unsigned char *sess_key;
			unsigned char *iv, *src, *dst;
			
			//standard input from sgs
			host_fd = elem->out_sg[1].iov_base;
			cfd = *host_fd;
			cmd = elem->out_sg[2].iov_base;
```

Για τα sessions, εκτελούμε τις αντίστοιχες κλήσεις προς τη συσκευή
```C
case CIOCGSESSION:
				DEBUG("CIOCGSESSION");
				//Read from sgs
				sess_key = elem->out_sg[3].iov_base;
				sess_op = elem->in_sg[0].iov_base;
				//Populate values
				memset(&sess, 0 ,sizeof(sess));
				sess.keylen = sess_op->keylen;
				sess.cipher = sess_op->cipher;
				sess.key = sess_key;

				if(ioctl(cfd, CIOCGSESSION, &sess) < 0)
					DEBUG("Failed to start session");
				sess_op->ses = sess.ses;
				break;
			case CIOCFSESSION:
				DEBUG("CIOCFSESSION");
				//Read from sgs
				sess_id = elem->out_sg[3].iov_base;
				//Populate value
				_sess_id = *sess_id;
				if(ioctl(cfd, CIOCFSESSION, &_sess_id) < 0)
					DEBUG("Failed to end session");
```

Στην περίπτωση του crypt, αντίστοιχα παίρνουμε τα απαιτούμενα πεδία απο τα scatterlists και γράφουμε στο scatterlist του dst
```C
case CIOCCRYPT:
				DEBUG("CIOCCRYPT");

				//Read from sgs
				cryp_op = elem->out_sg[3].iov_base;
				iv = elem->out_sg[4].iov_base;
				src = elem->out_sg[5].iov_base;
				dst = elem->in_sg[0].iov_base;
				//Populate values
				memset(&cryp, 0, sizeof(cryp));
				cryp.ses = cryp_op->ses;
				cryp.len = cryp_op->len;
				cryp.iv = iv;
				cryp.src = src;
				cryp.dst = dst;
				cryp.op = cryp_op->op;

				if(ioctl(cfd, CIOCCRYPT, &cryp) < 0)
					DEBUG("Failed to do cryp operation");
				
			default: break;
```

Αφού τελειώσουμε με τις κλήσεις, επιστρέφουμε το ανανεωμένο vq element στο virtqueue.
```C
virtqueue_push(vq, elem, 0); 
```

Ο κώδικας του back-end αναπτύχθηκε στο αρχείο virtio-cryptodev.c


Μεσα στη συνάρτηση ```vq_handle_output``` υλοποιείται η συμπεριφορά του backend κατά την ενημερωση του από τo frontend για νέα δεδομένα στη virtqueue:
```C

static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq)
{
	VirtQueueElement *elem;
	unsigned int *syscall_type;

	DEBUG_IN();

	elem = virtqueue_pop(vq, sizeof(VirtQueueElement));
	if (!elem) {
		DEBUG("No item to pop from VQ :(");
		return;
	} 

	DEBUG("I have got an item from VQ :)");

	syscall_type = elem->out_sg[0].iov_base;
	int *cfd;
	//cfd = (int*)malloc(sizeof(*cfd));
	switch (*syscall_type) {

		case VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN");
			cfd =  elem->in_sg[0].iov_base;
			*cfd = open("/dev/crypto", O_RDWR);
			if (*cfd < 0) {
				DEBUG("could not open(/dev/crypto)");
			}
			DEBUG("Opened crypto dev");
			break;

		case VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE");
			cfd = elem->out_sg[1].iov_base;
			if(close(*cfd) < 0)
				DEBUG("could not close(/dev/crypto)");
			DEBUG("Closed crypto dev");
			break;

		case VIRTIO_CRYPTODEV_SYSCALL_TYPE_IOCTL:
			DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_IOCTL");
			//variables for fields
			int *host_fd;
			int cfd;
			unsigned int *cmd;

			struct session_op *sess_op;
			struct session_op sess;
			struct crypt_op *cryp_op;
			struct crypt_op cryp;

			uint32_t *sess_id;
			uint32_t _sess_id;
			unsigned char *sess_key;
			unsigned char *iv, *src, *dst;
			
			//standard input from sgs
			host_fd = elem->out_sg[1].iov_base;
			cfd = *host_fd;
			cmd = elem->out_sg[2].iov_base;

			switch(*cmd){
			case CIOCGSESSION:
				DEBUG("CIOCGSESSION");
				//Read from sgs
				sess_key = elem->out_sg[3].iov_base;
				sess_op = elem->in_sg[0].iov_base;
				//Populate values
				memset(&sess, 0 ,sizeof(sess));
				sess.keylen = sess_op->keylen;
				sess.cipher = sess_op->cipher;
				sess.key = sess_key;

				if(ioctl(cfd, CIOCGSESSION, &sess) < 0)
					DEBUG("Failed to start session");
				sess_op->ses = sess.ses;
				break;
			case CIOCFSESSION:
				DEBUG("CIOCFSESSION");
				//Read from sgs
				sess_id = elem->out_sg[3].iov_base;
				//Populate value
				_sess_id = *sess_id;
				if(ioctl(cfd, CIOCFSESSION, &_sess_id) < 0)
					DEBUG("Failed to end session");
			
			case CIOCCRYPT:
				DEBUG("CIOCCRYPT");

				//Read from sgs
				cryp_op = elem->out_sg[3].iov_base;
				iv = elem->out_sg[4].iov_base;
				src = elem->out_sg[5].iov_base;
				dst = elem->in_sg[0].iov_base;
				//Populate values
				memset(&cryp, 0, sizeof(cryp));
				cryp.ses = cryp_op->ses;
				cryp.len = cryp_op->len;
				cryp.iv = iv;
				cryp.src = src;
				cryp.dst = dst;
				cryp.op = cryp_op->op;

				if(ioctl(cfd, CIOCCRYPT, &cryp) < 0)
					DEBUG("Failed to do cryp operation");
				
			default: break;
			}
		break;	

		default:
			DEBUG("Unknown syscall_type");
			break;

	}

	virtqueue_push(vq, elem, 0);
	virtio_notify(vdev, vq);
	g_free(elem);
}
```


