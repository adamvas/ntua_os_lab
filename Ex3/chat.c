#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#define TCP_PORT    35001
#define TCP_BACKLOG 5


/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = write(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

void communicate(int sd,uint8_t clientorserver);

int main(int argc, char *argv[]){
    int sd, newsd, port;
    int cli_or_srv = 0;
    ssize_t n;
    char buf[100];
    char sendBuf[253];
    char addrstr[INET_ADDRSTRLEN];
    char *hostname;
    socklen_t len;
    struct hostent *hp;
    struct sockaddr_in sa;
    signal(SIGPIPE, SIG_IGN);

    if (argc != 3) {
            fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
            exit(1);
    }
    hostname = argv[1];
    port = atoi(argv[2]); /* Needs better error checking */

    /* Create TCP/IP socket, used as main chat channel */
    if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket");
            exit(1);
    }

    if ( !(hp = gethostbyname(hostname))) {
            printf("DNS lookup failed for host %s\n", hostname);
            exit(1);
    }

    /** CLIENT START **/
    /* Connect to remote TCP port */
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
    //fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
    if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
            printf("User is down, waiting...\n");
            goto TRY_SERVER;
    }
    cli_or_srv = 0;
    goto COMMUNICATE;
    /** CLIENT END **/
    
    /** SERVER START **/
    TRY_SERVER:
    /* Bind to a well-known port */
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
            perror("bind");
            exit(1);
    }
    //fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

    /* Listen for incoming connections */
    if (listen(sd, TCP_BACKLOG) < 0) {
            perror("listen");
            exit(1);
    }
    /* Accept when the other user attempts to connect */
    if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
            perror("accept");
            exit(1);
    }
    if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
            perror("could not format IP address");
            exit(1);
    }
    //fprintf(stderr, "Incoming connection from %s:%d\n", addrstr, ntohs(sa.sin_port));
    cli_or_srv = 1;
    goto COMMUNICATE;
    /** SERVER END **/
    
    COMMUNICATE:
    fprintf(stderr, "- - -Chat started- - -\n");
    if (cli_or_srv ==1) communicate(newsd, cli_or_srv);
    else communicate(sd,cli_or_srv);
    return 0;
} 

void listener_task(int sd);
void sender_task(int sd);

void communicate(int sd, uint8_t clientorserver){ //0= client , 1 = server

  pid_t listener = fork();
  if (listener == 0){
    listener_task(sd);
   return;
  }
  else if (listener<0){
      perror("fork for  listener");
      exit(1);
  }

    pid_t sender = fork();
    if (sender ==0){
        sender_task(sd);
        return;
    }
    else if (sender<0){
        perror("fork for sender");
        exit(1);
    }
    wait(NULL);
    if (clientorserver==0)shutdown(sd, SHUT_RDWR);
    else if (clientorserver == 1){
		sleep(0.1);
		close(sd);
	}
    kill(listener, SIGKILL);
    kill(sender, SIGKILL);
}
  
void sender_task(int sd){
     ssize_t n;
     char buf[100]; 
     char endmess[5];
     strcpy(endmess,"END\n");
     
     for (;;) {
            n = fgets(buf, 100, stdin);
            
            if (n <= 0) {
                if (n < 0)
                    perror("read from user input failed");
            }
            n = strlen(buf);
            //printf("Read %d from keyboard\n", n);
            if (insist_write(sd, buf,n) != n) {
                perror("cannot write to standard output");
                break;
            }
            if (strcmp(buf,endmess) == 0 ) break;
        }
   
}
    
void listener_task(int sd){
    ssize_t n;
    char buf[100]; 
    char endmess[5];
    strcpy(endmess,"END\n");
    char responcemess[20];
    strcpy(responcemess,"    Responce:\0");

    for (;;) {
        n = read(sd, buf, 100);
        if (n <= 0) {
            if (n < 0)
                perror("read from remote peer failed");
            else
                fprintf(stderr, "Peer went away\n");
            break;
        }
        if (insist_write(0, responcemess, strlen(responcemess)) != strlen(responcemess)) {
            perror("cannot write to standard output");
            break;
        }
        if (insist_write(0, buf, n) != n) {
            perror("cannot write to standard output");
            break;
        }
        
        if (strcmp(buf,endmess) == 0 ) break;

    }
}





