/*
 * crypto-chrdev.c
 *
 * Implementation of character devices
 * for virtio-cryptodev device 
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 *
 */
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "cryptodev.h"

/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device 
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
	struct crypto_device *crdev;
	unsigned long flags;

	debug("Entering");

	spin_lock_irqsave(&crdrvdata.lock, flags);
	list_for_each_entry(crdev, &crdrvdata.devs, list) {
		if (crdev->minor == minor)
			goto out;
	}
	crdev = NULL;

out:
	spin_unlock_irqrestore(&crdrvdata.lock, flags);

	debug("Leaving");
	return crdev;
}

/*************************************
 * Implementation of file operations
 * for the Crypto character device
 *************************************/

static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev;
	struct virtqueue *vq;
	unsigned int *syscall_type;
	int *host_fd;
	struct scatterlist syscall_type_sg, cfd_sg, *sgp[2];
	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_OPEN;
	host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = -1; //initialize the reply to -1 in case you dont get answer
	
	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));
	if (!crdev) {
		debug("Could not find crypto device with %u minor", 
		      iminor(inode));
		ret = -ENODEV;
		goto fail;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail;
	}

	//This is how we get the message back to end user
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;
	vq = crdev->vq;
	//sema_init(&crdev->sem, 1);

	/**
	 * We need two sg lists, one for syscall_type and one to get the 
	 * file descriptor from the host.
	 **/
	/* ?? */

	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	//sg_init_one(&cfd_sg, cfd, sizeof(cfd));
	sg_init_one(&cfd_sg,host_fd,sizeof(*host_fd));

	sgp[0] = &syscall_type_sg;
	sgp[1] = &cfd_sg;
	//lock
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;

	err = virtqueue_add_sgs(vq, sgp, 1, 1, &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);

	/**
	 * Wait for the host to process our data.
	 **/
	while(virtqueue_get_buf(vq, &len) == NULL);
	debug("The true fd is: %d", *host_fd);
	up(&crdev->sem);
	//end lock
	crof->host_fd = *host_fd;
	/* ?? */


	/* If host failed to open() return -ENODEV. */
	/* ?? */
		

fail:
	kfree(syscall_type);
	kfree(host_fd);
	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	unsigned int *syscall_type;
	int *host_fd;
	struct scatterlist syscall_type_sg, cfd_sg, *sgp[2];
	struct virtqueue *vq;
	debug("Entering");

	vq = crdev->vq;
	
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_CLOSE;

	host_fd = kzalloc(sizeof(*host_fd),GFP_KERNEL);
	*host_fd = crof->host_fd;
	
	sg_init_one(&syscall_type_sg,syscall_type,sizeof(syscall_type));
	sg_init_one(&cfd_sg,host_fd,sizeof(*host_fd));
	
	sgp[0]=&syscall_type_sg;
	sgp[1]=&cfd_sg;
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;

	err = virtqueue_add_sgs(vq,sgp,2,0,&syscall_type_sg,GFP_ATOMIC);
	virtqueue_kick(vq);
	
	while(virtqueue_get_buf(vq,&len) == NULL);
	debug("finished closing the crypto fd");

	up(&crdev->sem);
	/**
	 * Send data to the host.
	 **/
	/* ?? */

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */

	//kfree(crof);    ???this was here but i read that  you should use kfree only after kmalloc
	kfree(syscall_type);
	kfree(host_fd);
	debug("Leaving");
	return ret;

}

static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd, 
                                unsigned long arg)
{
	long ret = 0;
	int err;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	unsigned int num_in, num_out;

	//Variables for fields
	struct session_op *sess_op;
	struct crypt_op *cryp_op;
	
	unsigned int len;

	unsigned int *syscall_type;
	unsigned int *mycmd;
	int *cfd;
	
	unsigned char *sess_key;
	u_int32_t *sess_id;
	unsigned char *iv, *dst, *src;

	//Their scatterlists
	struct scatterlist syscall_type_sg, cfd_sg, mycmd_sg;
	struct scatterlist sess_id_sg, sess_op_sg, sess_key_sg;
	struct scatterlist cryp_op_sg, iv_sg, src_sg, dst_sg;
	struct scatterlist *sgs[10];

	debug("Entering");

	/**
	 * Standard Inits
	 **/
	num_in = 0;
	num_out = 0;
	syscall_type = kmalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_IOCTL;

	mycmd = kmalloc(sizeof(*mycmd), GFP_KERNEL);
	*mycmd = cmd;

	cfd = kmalloc(sizeof(*cfd), GFP_KERNEL);
	*cfd = crof->host_fd;

	/**
	 * Extra Inits
	 **/
	sess_op = kzalloc(sizeof(*sess_op), GFP_KERNEL);
	sess_id = kzalloc(sizeof(*sess_id), GFP_KERNEL);
	sess_key = kmalloc(sizeof(unsigned char), GFP_KERNEL);

	cryp_op = kzalloc(sizeof(*cryp_op), GFP_KERNEL);
	
	
	iv = kzalloc(sizeof(unsigned char), GFP_KERNEL);
	src = kzalloc(sizeof(unsigned char), GFP_KERNEL);
	dst = kzalloc(sizeof(unsigned char), GFP_KERNEL);

	
	/**
	 * Standard sg Inits
	 **/
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sg_init_one(&cfd_sg, cfd, sizeof(*cfd));
	sg_init_one(&mycmd_sg, mycmd, sizeof(*mycmd));

	/**
	 * Standard sg attaches
	 **/
	sgs[0] = &syscall_type_sg;
	sgs[1] = &cfd_sg;
	sgs[2] = &mycmd_sg;
	

	switch (*mycmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		//copy struct from userspace
		if(copy_from_user(sess_op, (void __user *)arg, sizeof(*sess_op)))
				return -EFAULT;
		//copy array from userspace using the struct pointer
		kfree(sess_key);
		sess_key = kmalloc(sess_op->keylen, GFP_KERNEL);
		if(copy_from_user(sess_key, sess_op->key, sess_op->keylen))
				return -EFAULT;
		//init specific sg
		sg_init_one(&sess_key_sg, sess_key, sess_op->keylen);
		sg_init_one(&sess_op_sg, sess_op, sizeof(*sess_op));
		//attach specifig sg
		sgs[3] = &sess_key_sg;
		sgs[4] = &sess_op_sg; //in

		//set out-in values
		num_out = 4;
		num_in = 1;

		//TODO add sg for return value of operation?
		break;

	case CIOCFSESSION:
		debug("CIOCFSESSION");
		if(copy_from_user(sess_id, (void __user *)arg, sizeof(*sess_id)))
			return -EFAULT;

		sg_init_one(&sess_id_sg, sess_id, sizeof(*sess_id));
		sgs[3] = &sess_id_sg;

		//set out-in values
		num_out = 4;
		num_in = 0;

		//TODO add sg for return value of operation?
		break;
		
	case CIOCCRYPT:
		debug("CIOCCRYPT");
		if(copy_from_user(cryp_op, (void __user *)arg, sizeof(*cryp_op)))
			return -EFAULT;
		//init specific arrays
		kfree(iv);
		kfree(src);
		kfree(dst);		
		
		iv = kmalloc(VIRTIO_CRYPTODEV_BLOCK_SIZE, GFP_KERNEL);
		src = kmalloc(cryp_op->len, GFP_KERNEL);
		dst = kmalloc(cryp_op->len, GFP_KERNEL);
		//populate arrays from user space
		if(copy_from_user(iv, cryp_op->iv, VIRTIO_CRYPTODEV_BLOCK_SIZE))
			return -EFAULT;
		if(copy_from_user(src, cryp_op->src, cryp_op->len))
			return -EFAULT;
		if(copy_from_user(dst, cryp_op->dst, cryp_op->len))
			return -EFAULT;
		debug("Userspace src: %s", src);
		//init specific sg
		sg_init_one(&cryp_op_sg, cryp_op, sizeof(*cryp_op));
		sg_init_one(&iv_sg, iv, VIRTIO_CRYPTODEV_BLOCK_SIZE);
		sg_init_one(&src_sg, src, cryp_op->len);
		sg_init_one(&dst_sg, dst, cryp_op->len);
		
		//attach specific sg
		sgs[3] = &cryp_op_sg;
		sgs[4] = &iv_sg;
		sgs[5] = &src_sg;
		sgs[6] = &dst_sg;
		
		//set out-in values
		num_out = 6;
		num_in = 1;
		break;

	default:
		debug("Unsupported ioctl command");

		break;
	}


	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	/* ?? Lock ?? */
	if(down_interruptible(&crdev->sem) < 0)
		return -ERESTARTSYS;
	err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		/* do nothing */;
	up(&crdev->sem);

	switch(*mycmd){
		case CIOCGSESSION:
			if(copy_to_user((void __user *)arg, sess_op, sizeof(*sess_op)))
				return -EFAULT;
		case CIOCFSESSION:
			break;
		case CIOCCRYPT:
			if (copy_to_user(cryp_op->dst, dst, cryp_op->len))
				return -EFAULT;
		default :
			break;
	}
	kfree(syscall_type);
	kfree(mycmd);
	kfree(cfd);
	kfree(sess_op);
	kfree(sess_id);
	kfree(sess_key);
	kfree(cryp_op);
	kfree(iv);
	kfree(src);
	kfree(dst);
	debug("Leaving");

	return ret;
}

static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf, 
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
	int ret;
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;
	
	debug("Initializing character device...");
	cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
	crypto_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
	if (ret < 0) {
		debug("failed to register region, ret = %d", ret);
		goto out;
	}
	ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device");
		goto out_with_chrdev_region;
	}
	
	debug("Completed successfully");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
	return ret;
}

void crypto_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("entering");
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	cdev_del(&crypto_chrdev_cdev);
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
	debug("leaving");
}
