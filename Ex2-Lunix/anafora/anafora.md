[//]: <> (pandoc --pdf-engine=xelatex --highlight-style tango anafora.md -o anafora.pdf -V mainfont="DejaVu Serif" -V monofont="DejaVu Sans Mono" -V geometry:"top=3cm, bottom=2.3cm, left=2.5cm, right=2.5cm")

---
header-includes:
 - \usepackage{fvextra}
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
 
 
 - \definecolor{bgcolor}{HTML}{f8f8f8}
 - \let\oldtexttt\texttt

 - \renewcommand{\texttt}[1]{
  \colorbox{bgcolor}{\oldtexttt{#1}}
  } 

---

![ ](./pyrforos.png )

# Εθνικό Μετσόβιο Πολυτεχνείο 
## Σχολή Ηλεκτρολόγων Μηχανικών και Μηχανικών Υπολογιστών 
## Εργαστήριο Λειτουργικών Συστημάτων, 2019-2020 

***

## 2η Εργαστηριακή Άσκηση:
## Οδηγός Ασύρματου Δικτύου Αισθητήρων στο λειτουργικό σύστημα Linux
#### Ονοματεπώνυμα: Αδαμόπουλος Βασίλειος, Τσαγκαρέλης Γεώργιος
#### Α.Μ.: 03116695, 03116674

***

Στην παρούσα έργασία υλοποιήθηκε ένας οδηγός συσκευής χαρακτήρων(character device) ως μέρος ενός οδηγού συσκευής  για  ένα  ασύρματο  δίκτυο  αισθητήρων  κάτω  από  το  λειτουργικό  σύστημα  Linux.

Συγκεκριμένα τα δεδομένα από το δίκτυο αισθητήρων λαμβάνονται από τη σειριακή μέσω μιας διάταξης γραμμής και τίθενται σε επεξεργασία από άλλα κομμάτια του οδηγού.
O εν λόγω οδηγός συσκευής χαρακτήρων αναλαμβάνει να παραλάβει τα δεδομένα τα οποία προετοιμάζονται από τα άλλα κομμάτια οτυ οδηγού και να τα παρουσιάσει στο χρήστη με τη μορφή αρχείων. Δημιουργείται ένα αρχείο για κάθε μια από τις 3 μετρήσεις(μπαταρια,θερμοκρασια,φωτεινότητα) για κάθε αισθητηρα.

***

##### Παρακάτω παρουσιάζεται η δομή και η λειτουργία του οδηγου:

* Η συνάρτηση `lunix_chrdev_state_needs_refresh` υλοποιεί τον έλεγχο για τον αν χρειάζεται ανανέωση των δεδομένων που φυλασσονται στη δομη `state` που αστιστοιχεί σε κάθε ανοικτό από τον χρήστη αρχείο. Δεω χρειάζεται να γίνει κάποιο κλείδωμα καθώς η διαδικασία ελέγχου είναι πολύ γρήγορη και δεν κάνει καμία εγγραφή σε μνήμη. 


```C
/*
 * Just a quick [unlocked] check to see if the cached
 * chrdev state needs to be updated from sensor measurements.
 */
static int lunix_chrdev_state_needs_refresh(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
	
	WARN_ON ( !(sensor = state->sensor));

	int ret;
	ret = 0;
	if((state->buf_timestamp < sensor->msr_data[state->type]->last_update))
	{
		ret = 1;
	}
	return ret;
}
```

* Η συνάρτηση `lunix_chrdev_state_update` ανανεώνει τα δεδομένα που είναι αποθηκευμένα στη δομή, εφόσον αυτό προκύψει ότι χρειάζεται μετά από μια κλήση της `lunix_chrdev_state_needs_refresh`. Κατά την αντιγραφή των δεδομένων από τον αισθητήρα κλειδώνουμε το spinlock του συγκεκριμένου αισθητήρα ώστε να είμαστε σίγουροι ότι τα δεδομένα που υπάρχουν μεσα στο `sensor` struct να μην αλλάξουν (π.χ. από άλλα μέρη του οδηγού) κατα τη διάρκεια της δικής μας ανάγνωσης. Επίσης η κλήση αυτής της συνάρτησης πρέπει να γίνει με κλειδωμένο τον σεμαφόρο του state καθώς γίνονται αλλαγές σε πεδία του `state` struct που μπορεί να προσπελαστούν από διεργασίες (γονικές/παιδιά) οι οποίες έχουν το ίδιο `state` struct.

```C
/*
 * Updates the cached state of a character device
 * based on sensor data. Must be called with the
 * character device state lock held.
 */
static int lunix_chrdev_state_update(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
  	WARN_ON ( !(sensor = state->sensor));
	/*
	 * Grab the raw data quickly, hold the
	 * spinlock for as little as possible.
	 */
	
	/* Why use spinlocks? See LDD3, p. 119 */

	/*
	 * Any new data available?
	 */

	
	uint32_t tempData;
	tempData = 0;
	if(lunix_chrdev_state_needs_refresh(state))
	{
		spin_lock(&(sensor->lock));
		memcpy(&tempData, &(sensor->msr_data[state->type]->values[0]), sizeof(sensor->msr_data[state->type]->values[0]));
		state->buf_timestamp = sensor->msr_data[state->type]->last_update;
		spin_unlock(&(sensor->lock));
		//debug("Completed: tempData is %d", tempData, state->buf_timestamp);

	}
	else
	{
		return -EAGAIN;
	}

	/*
	 * Now we can take our time to format them,
	 * holding only the private state semaphore
	 */
		
	long result;
	result = 0;
	
	switch(state->type)
	{
		case BATT:
			if(tempData < 65536)
		       	result = lookup_voltage[tempData];	
			break;
		case TEMP:
		       	if(tempData < 65536)	
			result = lookup_temperature[tempData];
			break;
		case LIGHT:
			if(tempData < 65536)
			result = lookup_light[tempData];
			break;
		default:
			break;
	}
	
	int decimal;
	decimal = result % 1000;
	int integer;
	integer = result / 1000;
	sprintf(&(state->buf_data[0]), "%d.%d\n", integer, decimal);
	debug("Got value: %s\n", state->buf_data);

	return 0;
}
```

* Η συνάρτηση `lunix_chrdev_open` αρχικοποιεί το περιβάλλον ώστε να μπορεί ο χρήστης να κάνει ασφαλή και λειτουργική χρήση της read. Η συνάρτηση κοιτά σε ποιόν αισθητήρα θέλει ο χρήστης να αποκτήσει πρόσβαση μέσω των major/minor numbers. Έπειτα δημιουργεί και αρχικοποιεί ένα κατάλληλο `lunix_chrdev_state_struct` το οποίο αναφέρεται στον αισθητήρα αυτόν.
Τέλος, επιστρέφει στον χώρο του χρήστη έναν file pointer. 

```C
/*************************************
 * Implementation of file operations
 * for the Lunix character device
 *************************************/

static int lunix_chrdev_open(struct inode *inode, struct file *filp)
{
	/* Declarations */
	int major;
	int minor;
	int ret;
	int sensorNum;
	int sensorType;
	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto out;

	/*
	 * Associate this open file with the relevant sensor based on
	 * the minor number of the device node [/dev/sensor<NO>-<TYPE>]
	 */
	major = imajor(inode);
	minor = iminor(inode);
	debug("Opening minor %d\n", minor);
	
	/* Allocate a new Lunix character device private state structure */
	sensorNum = minor / 8;
	sensorType = minor % 8;

	struct lunix_chrdev_state_struct *state;
	state = kmalloc(sizeof(struct lunix_chrdev_state_struct), GFP_KERNEL);
	memset(state, 0, sizeof(state));
	state->type = sensorType;
	state->sensor = &(lunix_sensors[sensorNum]);
	sema_init(&state->lock, 1);

	filp->private_data = state;
		
out:
	return ret;
}
```
* Η συνάρτηση `lunix_chrdev_release` ευθύνεται για την ασφαλή αποδέσμευση ενός αρχειόυ κατά την κλήση `close` από τον χρήστη . Αυτό σημαίνει απελευθέρωση της μνήμης που χρησιμοποιήθηκε για την εξυπηρέτηση της διαδικασίας αυτής. Όπως είδαμε στην read, δημιουργήθηκε ένα struct για την ανάγνωση δεδομένων, επομένως πρέπει να αποδεσμεύσουμε την μνήμη που δεσμεύσαμε, το οποίο επιτυγχάνουμε με την κλήση kfree.

```C
static int lunix_chrdev_release(struct inode *inode, struct file *filp)
{
	/* not sure if we need these */
	int major;
	int minor;
	major = imajor(inode);
	minor = iminor(inode);
	/* */
	kfree(filp->private_data);
	debug("Released %d\n", minor);
	return 0;
}
```

* Η συνάρτηση `lunix_chrdev_read` είναι η συνάρτηση που πραγματοποιεί την ομαλή (και συνεκτική) ανάγνωση δεδομένων απο τον αισθητήρα. Επιτυγχάνει ενός τέτοιου είδους ανάγνωση με τον εξής τρόπο: 
Με τη χρήση του `f_pos` γνωρίζουμε σε ποιό σημείο της πρότασης βρισκόμαστε στην συγκεκριμένη κλήση της read. (αυτό συμβαίνει γιατί κάποιος θα μπορούσε να διαβάζει μέσω της read τα δεδομένα ανά x byte συνεχώς)
Εάν αυτός ο μετρητής είναι ίσος με 0, δηλαδή είμαστε στην αρχή της πρότασης, θα γίνει κλήση ενός κατάλληλου loop το οποίο πραγματοποιεί ανανέωση των δεδομένων της πρότασης που θα διαβαστεί. Εώς ότου έρθουν νέα δεδομένα για ανανέωση, ο διεργασία μπαίνει σε κατάσταση sleep, καθώς και σε μία ουρά στην οποία βρίσκονται όλες οι διεργασίες που επιθυμούν να κάνουν update παράλληλα. Η κεφαλή της ουράς αυτής (δηλαδή μια διεργασία) ξυπνά από τη δομή του αισθητήρα όταν καταφθάσουν σε αυτόν νέα δεδομένα. Όταν μια διεργασία ξυπνήσει, αποκτά τα κατάλληλα locks και πραγματοποιεί update των δεδομένων της πρότασης.
Τέλος, γίνεται η μεταφορά των δεδομένων στον χώρο χρήστη.

```C
static ssize_t lunix_chrdev_read(struct file *filp, char __user *usrbuf, size_t cnt, loff_t *f_pos)
{
	ssize_t ret;

	struct lunix_sensor_struct *sensor;
	struct lunix_chrdev_state_struct *state;
	int upd_ret;
	state = filp->private_data;
	WARN_ON(!state);

	sensor = state->sensor;
	WARN_ON(!sensor);
	ret = 0;
	/* Lock? */
	/*
	 * If the cached character device state needs to be
	 * updated by actual sensor data (i.e. we need to report
	 * on a "fresh" measurement, do so
	 */
	if(down_interruptible(&state->lock))
		return -ERESTARTSYS;
	if (*f_pos == 0) {
		while (lunix_chrdev_state_update(state) == -EAGAIN) {
			up(&state->lock);
			if(wait_event_interruptible(sensor->wq, (lunix_chrdev_state_needs_refresh(state))))
				return -ERESTARTSYS;
			if(down_interruptible(&state->lock))
				return -ERESTARTSYS;
			/* The process needs to sleep */
			/* See LDD3, page 153 for a hint */
		}
	}
	/* End of file */	
	/* Determine the number of cached bytes to copy to userspace */
	/* Auto-rewind on EOF mode? */
	
	int i;
	i = 0;
	int flag;
	flag = 1;
	while(flag){
		if(i==19){
			state->buf_data[19] = '\0';
			break;
		}
		if(state->buf_data[i] != '\0') i++;
		else{
			i++;
			break;
		}
	}
	
	int bytesToCopy = cnt;
	if((*f_pos + cnt) > i){
		bytesToCopy = i - *f_pos;
	}


	if(copy_to_user(&usrbuf[0], &(state->buf_data[*f_pos]), bytesToCopy))
		return -EFAULT;
	

	*f_pos += bytesToCopy;
	if(*f_pos == i) *f_pos = 0;
	ret = bytesToCopy;
	up(&state->lock);
out:
	return ret;
}
```
* Στο struct `lunix_chrdev_fops` ορίζονται οι συναρτήσεις που θα πρέπει να καλεστούν απο τον Kernel όταν ο χρήστης κάνει τις προβλεπόμενες κλήσεις `open` `read` και `close` σε ένα lunix character device 

```C
static struct file_operations lunix_chrdev_fops = 
{
        .owner          = THIS_MODULE,
	.open           = lunix_chrdev_open,
	.release        = lunix_chrdev_release,
	.read           = lunix_chrdev_read,
	.unlocked_ioctl = lunix_chrdev_ioctl,
	.mmap           = lunix_chrdev_mmap
};
```

* Η συνάρτηση `lunix_chrdev_init` αρχικοποιεί το character device στο σύστημα. Συγκεκριμένα, αρχικοποιεί την συσκευή και έπειτα κάνει allocate τον κατάλληλο χώρο για αυτή.
Έπειτα προσθέτει στο σύστημα τη συσκευή έχωντας τα δύο προηγούμενα στοιχεία έτοιμα. Εάν κάποια κλήση συστήματος επέστρεψε κωδικό σφάλματος, τότε η συνάρτηση φροντίζει να τον προωθήσει κατάλληλα.

```C
int lunix_chrdev_init(void)
{
	/*
	 * Register the character device with the kernel, asking for
	 * a range of minor numbers (number of sensors * 8 measurements / sensor)
	 * beginning with LINUX_CHRDEV_MAJOR:0
	 */
	int ret;
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;
	debug("initializing character device\n");
	cdev_init(&lunix_chrdev_cdev, &lunix_chrdev_fops);
	lunix_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);

	ret = register_chrdev_region(dev_no, lunix_minor_cnt, "lunix_driver");

	if (ret < 0) {
		debug("failed to register region, ret = %d\n", ret);
		goto out;
	}

	ret = cdev_add(&lunix_chrdev_cdev, dev_no, lunix_minor_cnt);	

	/* cdev_add? */
	if (ret < 0) {
		debug("failed to add character device\n");
		goto out_with_chrdev_region;
	}
	debug("completed successfully\n");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
out:
	return ret;
}
```

* Η συνάρτηση `lunix_chrdev_destroy` είναι υπεύθυνη για την διαγραφή ενός character device από το σύστημα.

```C
void lunix_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;
		
	debug("entering\n");
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	cdev_del(&lunix_chrdev_cdev);
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
	debug("leaving\n");
}

```

