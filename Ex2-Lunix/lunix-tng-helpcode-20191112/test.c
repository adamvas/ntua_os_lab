#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
int main(int argc, char **argv){
	int fd1;
	char data[20];
	memset(data, 0, 20 * sizeof(char));
	fd1 = open("/dev/lunix1-batt", O_RDONLY, 0640);
	int flag = 1;
	int count;
	while(flag){
		count = read(fd1, &data[0], 20);
		printf("I got %d bytes: %s \n", count, data);
		sleep(1);
	}
	close(fd1);
	return 0;
}
